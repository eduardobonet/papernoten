# Papernoten

Papers that I am reading or want to read.

Each paper is added as an issue:

- ~"status::backlog" are papers that I find interesting that I want to get to at some point
- ~"status::read_again" Papers that I read once to get a grasp, but I want to dive deeper
- ~"status::read" Papers that I read
- ~"status::learnings" After reading, I plan on adding my learnings as a comment to the issue

For those who want to get notified, you can subscribe on GitLab the issue RSS, or to a specifific label under `Manage > Labels`. 